Command line tool used to flood a targeted destination MAC/IP/port with
the specified number of UDP/IP/802.1Q Ethernet packets. The default length
of the UDP payload is 1400 bytes. The length of the payload may be set by
the user as an option on the command line. The source MAC/IP/port of the
packets are set in accordance with command line inputs. The IP layer
QoS field is set in accordance with command line input. The 802.1Q Ethernet
layer QoS field (i.e. 3-bit priority) and VLAN ID are also set in accordance
with command line inputs.

    Copyright (c)  2006  Mark D. Collier/Mark O'Brien
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.2
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".
 
Authors:  Mark D. Collier/Mark O'Brien - 06/09/2006  v2.0
          Mark D. Collier/Mark O'Brien - 10/04/2004  v1.0
          www.securelogix.com - mark.collier@securelogix.com
          www.hackingexposedvoip.com

This tool was produced with honorable intentions, which are:

  o To aid owners of VoIP infrastructure to test, audit, and uncover security
    vulnerabilities in their deployments.

  o To aid 3rd parties to test, audit, and uncover security vulnerabilities
    in the VoIP infrastructure of owners of said infrastructure who contract
    with or otherwise expressly approve said 3rd parties to assess said
    VoIP infrastructure.

  o To aid producers of VoIP infrastructure to test, audit, and uncover security
    vulnerabilities in the VoIP hardware/software/systems they produce.

  o For use in collective educational endeavors or use by individuals for
    their own intellectual curiosity, amusement, or aggrandizement - absent
    nefarious intent.
   
Unlawful use of this tool is strictly prohibited.

The following open-source libraries of special note were used to build
udpfloodVLAN:

1) libnet v1.1.2.1 (tool requires at least this version)
2) hack_library [utility routines - Str2IP( ) DumpPacket( )]
   see www.hackingexposedvoip.com

    Note: The Makefile for udpfloodVLAN presumes
          that hack_library.o and hack_library.h reside in
          a folder at ../hack_library relative to the Makefile
          within the udpfloodVLAN directory.

Install and build the libraries in accordance with their respective
instructions. Then change to the redirectpoison directory and type: make

The network layer QoS is the IP header TOS byte, more recently
redefined as the DSCP byte. The link layer QoS is the 3-bit user_priority
field found in the Ethernet 802.1Q VLAN specification. An Ethernet
compliant with the 802.1Q specification inserts 4 bytes into the
Ethernet MAC frame immediately following the source MAC address. The first two
bytes are a tag indicating the frame is 802.1Q compliant (i.e. 0x8100).
The next two bytes have: a user_priority in the upper 3-bits,
a 1-bit Canonical Format Indicator, and a 12-bit Virtual LAN ID (VLAN).
Since the link layer header is built explicitly by the tool,
the destination and source MAC addresses are required command line inputs.
The operator may also now optionally specify the size of the UDP payload
from 1 to 1400 bytes and may optionally specify verbose mode output.

Make the Makefile and you should be ready to go. You might receive
warnings based on the compiler vintage.

udpfloodVLAN - Version 2.0
               June 09, 2006
 Usage:
 Mandatory -
        interface (e.g. eth0)
        srcMAC  (xx:xx:xx:xx:xx:xx)
        destMAC (xx:xx:xx:xx:xx:xx)
        srcIPv4Addr  (ddd.ddd.ddd.ddd)
        destIPv4Addr (ddd.ddd.ddd.ddd)
        srcPort  (0 - 65535)
        destPort (0 - 65535)
        IP Type Of Service (TOS) value (0x00 - 0xFF)
        Ethernet user_priority (e.g. 0 - 7)
        VLAN ID (e.g. 0 - 4095)
        flood stage (i.e. number of packets)
 Optional -
        -s UDP payload size in bytes (e.g. 1 - 1400) [default 1400]
        -v verbose output mode
 
The background information which follows is from various an on-line sources
and not subject to the Copyright above:

---------------------------

Ethernet link layer mapping

Ethernet can carry a 3-bit user_priority for each frame.

This field was originally defined in IEEE 802.1p. The field is also present
in the 802.1Q <http://standards.ieee.org/getieee802/download/802.1Q-1998.pdf>
VLAN header.

Ethernet traffic types

Annex H of 802.1D contains a discussion of ethernet traffic types. These are,
in increasing order of network service:

user_priority	 Traffic type	 
    1             Background	 
    2             Spare	 
    0             Best effort	 
    3             Excellent effort	 
    4             Controlled load	 
    5             Video (<100mS latency)	 
    6             Voice (<10mS latency)	 
    7             Network control	 

Untagged frames have a user_priority of 0.

Note that user_priority = 1 and user_priority = 2 have worse service than
Best Effort.

Mapping DSCP to user_priority in AARNet

There is no standard mapping DSCP to user_priority. AARNet uses the
following mapping:

DiffServ Service      DSCP    Ethernet traffic class   user_priority	 
Scavenger              8         Background                  1	 
Best effort            0          Default                    0	 
Authenticated video    36          Video                     5	 
Voice                  40          Voice                     6	 
Control              48, 56     Network control              7	 

The following values are recommended for future services.

DiffServ Service     DSCP    Ethernet traffic class    user_priority	 
Assured forwarding   AFx       Controlled load               4	 

Switches and priority

Ethernet switches can implement all eight priority queues or fewer queues,
right down to one queue. It is important to determine how many queues your
site's ethernet switches have, as this determines to what extent the
user_priority of a frame has any significance.

If fewer than eight queues are implemented then 802.3 requires that the queues
are used for particular user_priority frames.

                                   Number of queues	 
User priority	 1	 2	 3	 4	 5	 6	 7	 8	 
      0	 	 0	 0	 0	 1	 1	 1	 1	 2	 
      1	 	 0	 0	 0	 0	 0	 0	 0	 0	 
      2	 	 0	 0	 0	 0	 0	 0	 0	 1	 
      3	 	 0	 0	 0	 1	 1	 2	 2	 3	 
      4	 	 0	 1	 1	 2	 2	 3	 3	 4	 
      5		 0	 1	 1	 2	 3	 4	 4	 5	 
      6		 0	 1	 2	 3	 4	 5	 5	 6	 
      7	 	 0	 1	 2	 3	 4	 5	 6	 7	 

For example, a switch with only two queues will place frames with a
user_priority of 0, 1, 2 or 3 in to the lower priority queue and frames
with a user_priority of 4, 5, 6 or 7 into the higher priority queue.

On an AARNet network this places Discard at Edge, Scavenger, Discard at
Charged and Best Effort traffic into the low priority queue and Voice and
Control traffic into the high priority queue. This is the correct outcome.


Campus design considerations


Trusted machines which generate QoS traffic should connect using a 802.1Q VLAN
trunk. This allows the machine to correctly set the ethernet user_priority as
the traffic crosses the campus's switched infrastructure.

Examples of these machines are PABX-VoIP gateways or H.323 proxies.

There is some advantage to switches which queue based upon the IP DSCP rather
than the Ethernet user_priority. These allow machines which use untagged
packets to gain link layer priority, at least to untrusted QoS services.

----------------------------------------------------

From: http://www.aarnet.edu.au/engineering/networkdesign/qos/precedence.html

Precedence and DSCP

There have been two generations of quality of service architectures in the
Internet. The interpretation of the Type of Service Octet in the Internet
Protocol header varies between these two generations.

The Type of Service Octet is the second 8-bit byte of the Internet Protocol
header.

 
2^0    2^4      2^8           2^16                         2^31
+=======+=======+==============+============================+
|       |       |              |                            | 
|Version|Header |   Type of    |       Total Length         |
|       |Length |   Service    |                            |
+=======+=======+==============+===+========================+
|                              |   |                        |
|       Identification         | DM|   Fragment Offset      |
|                              |0FF|                        |
+===============+==============+============================+
|               |              |                            |
|  Time to Live |    Protocol  |     Header Checksum        |     
|               |              |                            |
+===============+==============+============================+
|                                                           |
|                      Source Address                       |
|                                                           |
+===============+==============+============================+
|                                                           |
|                    Destination Address                    |
|                                                           |
+===============+==============+============================+

First generation

Precedence and type of service bits

The initial definition of the Type of Service Octet looked like this:

  2^7  2^6  2^5  2^4  2^3  2^2  2^1 2^0
+====+====+====+====+====+====+====+====+
|              |    |    |    |    |    |
|  Precedence  |  D |  T |  R |  C |    |
|              |    |    |    |    |    |
+====+====+====+====+====+====+====+====+

 RFC791 <http://www.ietf.org/rfc/rfc791.txt> gives Precedence the values
 in the table below

Precedence	 Purpose	 
    0          Routine	 
    1          Priority	 
    2          Immediate	 
    3          Flash	 
    4          Flash Override	 
    5          CRITIC/ECP	 
    6          Internetwork Control	 
    7          Network Control	 

Most Precedence descriptions are obscure: they relate to message handling 
riorities of US military communications in the 1960s. The essence is that
higher values of Precedence lead to higher levels of network service.

To prevent high link utilisation causing routing traffic to be lost it is
traditional to use Precedence = 7 for interior routing protocols such as OSPF
and RIP and to use Precedence = 6 for exterior routing protocols such as BGP.

The D type of service bit is defined in RFC791. A value of 0 requests normal
delay, a value of 1 requests a low delay service.

The T type of service bit is defined in RFC791. A value of 0 requests normal
througput, a value of 1 requests a high througput service.

The R type of service bit is defined in RFC791. A value of 0 requests normal
reliability, a value of 1 requests a high reliability service.

The C type of service bit is defined in RFC1394. A value of 0 requests normal
costs, a value of 1 requests a low cost service.

Precedence and type of service field

RFC1349 <http://www.ietf.org/rfc/rfc1349.txt> also made a slight change to the
definition of the Type of Service Octet, redefining it so that only one of the
D, T, R and C bits can be set. The RFC also renamed the group of bits the
Type of Service Field.

  2^7  2^6  2^5  2^4  2^3  2^2  2^1 2^0
+====+====+====+====+====+====+====+====+
|              |                   |    |
|  Precedence  |  Type of Service  |    |
|              |      Field        |    |
+====+====+====+====+====+====+====+====+

 Second generation


Differentiated services code point


The Differentiated Service Code Point is a selector for router's per-hop
behaviours. As a selector, there is no implication that a numerically greater
DSCP implies a better network service.
RFC2474 <http://www.ietf.org/rfc/rfc2474.txt> redefined the Type of Service 
octet to be:

 
  2^7  2^6  2^5  2^4  2^3  2^2  2^1 2^0
+====+====+====+====+====+====+====+====+
|                             |    |    |
|    Differentiated Services  | ECT| CE |
|         Code Point          |    |    |
+====+====+====+====+====+====+====+====+

 
The fields ECT and CE are nothing to do with quality of service. They are
spare bits in the IP header used by Explicit Congestion Notification. See
RFC3168 <http://www.ietf.org/rfc/rfc3168.txt> for details.

As can be seen, the DSCP totally overlaps the old Precedence field. So if
values of DSCP are carefully chosen then backward compatibility can be achieved.

This lead to notions of "class", each class being the group of DSCPs with
the same Precedence value. Values within a class would offer similar network
services but with slight differences (perhaps differing levels of service
such as "gold", "silver" and "bronze"). Classes were initially defined as:

DSCP	 Precedence	 Purpose	 
0           0           Best effort	 
8           1           Class 1	 
16          2           Class 2	 
24          3           Class 3	 
32          4           Class 4	 
40          5           Express forwarding	 
48          6           Control	 
56          7           Control	 

Assured forwarding service

RFC2697 <http://www.ietf.org/rfc/rfc2697.txt> defined a "assured forwarding" 
service. This codified the idea of "bronze", "silver" and "gold" levels of
network service above the Best Effort service.

DSCP      Service	 
0        Best effort	 
8        Class 1	 
10       Class 1, gold (AF11)	 
12       Class 1, silver (AF12)	 
14       Class 1, bronze (AF13)	 
16       Class 2	 
18       Class 2, gold (AF21)	 
20       Class 2, silver (AF22)	 
22       Class 2, bronze (AF23)	 
24       Class 3	 
26       Class 3, gold (AF31)	 
27       Class 3, silver (AF32)	 
30	 Class 3, bronze (AF33)	 
32	 Class 4	 
34	 Class 4, gold (AF41)	 
36	 Class 4, silver (AF42)	 
38	 Class 4, bronze (AF43)	 
40	 Express forwarding	 
48	 Control	 
56	 Control	 

Of course, the IETF couldn't use "loaded" terms like Gold, Silver and
Bronze to describe networks services. Apparently numbers are less loaded,
so the the RFC has descriptions like "AF21" rather than "Class 2, Gold".

The actual values of the DSCP for the Assured Forwarding service make sense
once you realise that they leave the last bit of the DSCP as 0 to allow for
possible future developments with the Assured Forwarding service.

Expedited forwarding service

RFC2598 <http://www.ietf.org/rfc/rfc2598.txt> defined a "expedited forwarding"
service. This service is designed to allow ISPs to offer a service with
attributes similar to a "leased line". This service offers the ultimate in
low loss, low latency and low jitter by ensuring that there is always sufficent
room in output queues for the contracted expedited forwarding traffic.

The Expedited Forwarding service has a DSCP of 46.

DSCP      Service	 
0        Best effort	 
8        Class 1	 
10       Class 1, gold (AF11)	 
12       Class 1, silver (AF12)	 
14       Class 1, bronze (AF13)	 
16       Class 2	 
18       Class 2, gold (AF21)	 
20       Class 2, silver (AF22)	 
22       Class 2, bronze (AF23)	 
24	 Class 3	 
26	 Class 3, gold (AF31)	 
27	 Class 3, silver (AF32)	 
30	 Class 3, bronze (AF33)	 
32	 Class 4	 
34	 Class 4, gold (AF41)	 
36	 Class 4, silver (AF42)	 
38	 Class 4, bronze (AF43)	 
40	 Express forwarding	 
46	 Expedited forwarding (EF)	 
48	 Control	 
56	 Control	 

Real world DSCPs

Voice

Most voice over IP implementations use Precedence = 5.

Discard

Cisco Systems' used DSCP = 1 in Cisco Security Advisory:
"Code Red" Worm - Customer Impact
<http://www.cisco.com/en/US/products/sw/voicesw/ps556/products_security_advisory09186a00800b1390.shtml> to mark Code Red traffic. The traffic would later be dropped.

Cisco Systems' Auto QoS

Cisco Systems' use standard DSCPs for their self-configuring QoS switch ports.

802 CoS	 DSCP         Use	 
0         0      Best effort	 
3         26     Voice control (SIP, H.323)	 
5         46     Voice data (RTP, RTSP)	 
2         18     Better effort data	 
2         18     Better effort	 
1         10     Streaming video	 
6         48     Network-layer control (OSPF, RIP, EIGRP)	 
7         -      Link-layer control (STP, CDP, UDLD)	
