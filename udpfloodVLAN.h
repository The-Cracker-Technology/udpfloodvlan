//-------------------------------------------------------------------------------
//
// udpfloodVLAN.h - Command line tool to attempt to flood
//              the specified destination MAC with the specified
//              number of UDP packets. Command line parms
//              permit the TOS byte in the IP Header, and the
//              user-priority field and the VLAN ID of the 802.1Q
//              Ethernet header to be set explicitly.
//
//    Copyright (C) 2006  Mark D. Collier/Mark O'Brien
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
//   Author: Mark D. Collier/Mark O'Brien - 06/09/2006  v2.0
//                   Mark D. Collier/Mark O'Brien - 10/04/2004  v1.0
//         www.securelogix.com - mark.collier@securelogix.com
//         www.hackingexposedvoip.com
//
//-------------------------------------------------------------------------------

#ifndef __UDPFLOODVLAN_H
#define __UDPFLOODVLAN_H

#include <libnet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h> 
#include <linux/sockios.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <sys/time.h>
#include <unistd.h>

#include <signal.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "hack_library.h"

#define __UDPFLOODVLAN_VERSION  "udpfloodVLAN - Version 2.0"
#define __UDPFLOODVLAN_DATE     "               June 09, 2006"

// Not really the maximum, but close to the Layer 2 limit once UDP, IP, and Ethernet headers
// are accounted for. The max Ethernet packet is about 1518 bytes.

#define __UDPFLOODVLAN_MAX_UDP_PAYLOAD_SIZE       1400
#define __UDPFLOODVLAN_DEFAULT_UDP_PAYLOAD_SIZE   __UDPFLOODVLAN_MAX_UDP_PAYLOAD_SIZE

#define __UDPFLOODVLAN_PATTERN_SIZE               4

unsigned char pattern[__UDPFLOODVLAN_PATTERN_SIZE] = { '\0', '\0', '\0', '\0' };

int i;
int opt;
int destPort;
int srcPort;
int len;
int numPackets;
int bytesWritten;
int ipPacketSize;
int etherPacketSize;
int srcIPv4Addr;
int destIPv4Addr;
int etherUserPriority;
int vlanID;
int ipTOS;
int rc;
int udpPayloadSize          = __UDPFLOODVLAN_DEFAULT_UDP_PAYLOAD_SIZE;

libnet_t      *l            = NULL;

libnet_ptag_t udp_tag       = 0;
libnet_ptag_t ip_tag        = 0;
libnet_ptag_t ether_tag     = 0;

u_int8_t *pSrcMAC           = NULL;
u_int8_t *pDestMAC          = NULL;

char *pEtherPacket          = NULL;

char *psDevice              = NULL;

char *psSrcMAC              = NULL;
char *psDestMAC             = NULL;

char *psSrcIPv4Addr         = NULL;
char *psDestIPv4Addr        = NULL;
char *psTempIPv4Addr        = NULL;

char *psUdpPayload          = NULL;

char errbuf[LIBNET_ERRBUF_SIZE];

bool bVerbose               = false;

void  catch_signals(int signo);
void  CleanupAndExit( int status );
void  usage();

#endif  //  __UDPFLOODVLAN_H

/* EOF */
